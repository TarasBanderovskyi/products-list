var myApp = angular.module('myApp',['ngStorage']);

myApp.controller('GreetingController', ['$scope', '$localStorage', function($scope, $localStorage) {
    
    $scope.products = [
    {name: "Milk", price: 12},
    {name: "Potato", price: 32},
    {name: "Orange", price: 16},
    {name: "Carrot", price: 17},
    {name: "Banana", price: 19},
    {name: "Sausages", price: 119},
    {name: "Yoghurt", price: 121},
    {name: "Cucumber", price: 162},
    ]  
    $scope.backetProducts = $localStorage.backetProducts || [];
    $scope.addProduct = function(product){      
      $scope.backetProducts.push(product);
      $localStorage.backetProducts = $scope.backetProducts;
    }
}]);